const controllers = {
    square(req, res) {
        const num = parseFloat(req.body);

        if (isNaN(num)) {
            return res.status(200).res.json({
                error: 'Invalid data entered',
                status: 200
            })
        }

        const result = Math.pow(num, 2);
        res.setHeader('Content-Type', 'application/json');
        res.status(200).json({number: num, square: result});

    },

    reverse(req, res) {

        const resReversed = req.body.split('').reverse().join('');
        res.setHeader('Content-Type', 'text/plain');
        res.status(200).send(resReversed);
    },

    date(req, res) {
        const { year, month, day } = req.params;
        const currentDate = new Date (new Date().setHours(0, 0, 0, 0));
        const definedData = new Date (year, month-1, day);
        const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        const defDay = days[definedData.getDay()];
        const fullYear=definedData.getFullYear();
        const isLeap = (fullYear % 4 === 0 && fullYear % 100 !== 0) || (fullYear % 400 === 0);
        const difference=Math.floor((Math.abs(currentDate.getTime() - definedData.getTime())) / (1000 * 3600 * 24));
        res.setHeader('Content-Type', 'application/json');
        res.status(200).json({weekDay: defDay,
            isLeapYear: isLeap,
            difference: difference
        })
    }
}

module.exports = controllers;